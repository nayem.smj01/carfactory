# CarFactory

# Custom Docker Images for  Project.

This project is about creating custom docker for Local Project.
These custom images will be used during CI/CD of Local projects.

**Pre-requisite** 

There should exists artificatory for storing these custom images.


**Environment Configuration**

As part of this project we need below env-variables to be created upfront with valid values. Gitlab CI & Terraform scripts will be reading this Configuration to deploy the database. PSB table for same - 


| Environment Variable Key  | Description |
| ------ | ------ |
| ARTIFACTORY_DOCKER_REPO    | This should have repository name |
| ARTIFACTORY_USERNAME | Should contain username for accessing repository |
| ARTIFACTORY_PASSWORD |Should contain password for accessing repository |

.

